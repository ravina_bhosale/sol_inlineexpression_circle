﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_InlineExpression_Circle_withDB.Entity
{
    public class PersonEntity
    {
        public String FirstName { get; set; }

        public String LastName { get; set; }
    }
}