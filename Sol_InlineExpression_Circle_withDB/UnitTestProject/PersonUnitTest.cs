﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sol_InlineExpression_Circle_withDB.DAL;

namespace UnitTestProject
{
    [TestClass]
    public class PersonUnitTest
    {
        [TestMethod]
        public void GetPersonTestMethod()
        {
            var result = new PersonDal().GetPersonData();

            Assert.IsNotNull(result);
        }
    }
}
