﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Person.aspx.cs" Inherits="Sol_InlineExpression_Circle_withDB.Person" %>

<%@ Import Namespace="Sol_InlineExpression_Circle_withDB.DAL" %>
<%@ Import Namespace="Sol_InlineExpression_Circle_withDB.Entity" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PersonData</title>

     <style>
        .circle
        {
              width: 180px;
              height: 180px;
              border-radius:160px;
              border-color:orange;
              margin:5px;
              padding:5px;
              border-style:solid;
              color:red;
              line-height:180px;
              text-align: center;
              font-size:large;
              background-color:darkslateblue;
              float:left;
        }
 

    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div>
        <%--Code Expreesion Block--%>

        <% 
            var selectPersonData = new PersonDal().GetPersonData();
        %>

        <%
            foreach (var obj in selectPersonData)
            {
        %>
             <div class="circle">
        
                  <span><% Response.Write(Server.HtmlEncode(obj.FirstName)); %></span>
                  <span><% Response.Write(Server.HtmlEncode(obj.LastName)); %></span>

             </div>
        <%
            }
                 %>

    </div>
    </form>
</body>
</html>
