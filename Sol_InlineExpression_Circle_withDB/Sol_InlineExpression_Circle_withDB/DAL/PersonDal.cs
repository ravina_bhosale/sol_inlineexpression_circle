﻿using Sol_InlineExpression_Circle_withDB.DAL.ORD;
using Sol_InlineExpression_Circle_withDB.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sol_InlineExpression_Circle_withDB.DAL
{
    public class PersonDal
    {
        #region Declaration

        private PersonDcDataContext _dcObj = null;

        #endregion

        #region Constructor

        public PersonDal()
        {
            _dcObj = new PersonDcDataContext();
        }

        #endregion

        #region Public Method

        public IEnumerable<PersonEntity> GetPersonData()
        {
            try
            {
                var getQuery =
                    _dcObj
                    ?.tblPersons
                    ?.AsEnumerable()
                    ?.Select((lelinqPersonObj) => new PersonEntity()
                    {
                        FirstName=lelinqPersonObj.FirstName,
                        LastName=lelinqPersonObj.LastName
                    })
                    .ToList();

                return getQuery;
            }
            catch (Exception)
            {
                throw;
            }

            #endregion
        }
    }
}